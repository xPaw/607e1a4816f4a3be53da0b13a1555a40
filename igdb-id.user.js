// ==UserScript==
// @name         Display game id on IGDB
// @namespace    https://www.igdb.com/games/
// @version      1.0
// @author       xPaw
// @match        https://www.igdb.com/games/*
// @icon         https://www.igdb.com/favicon.ico
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    const id = document.querySelector( 'meta[id=pageid]' );

    if( !id ) return;

    const el = document.createElement( 'span' );
    el.textContent = `ID: ${id.dataset.gameId}`;
    el.style.position = 'absolute';
    el.style.top = '10px';
    el.style.left = '10px';
    el.style.zIndex = 10000;
    el.classList.add( 'text-muted' );

    document.body.appendChild( el );
})();
